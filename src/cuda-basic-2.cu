/*
 ============================================================================
 Name        : cuda-basic-2.cu
 Author      : Georgiy Odisharia
 Version     :
 Copyright   : MIT Liscence
 Description : CUDA compute reciprocals
 ============================================================================
 */

#include <iostream>
#include <numeric>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static const int LENGTH	= 10;
static const int TIME 	= 5;
static const float DISCRETIZATION_LENGTH = 0.005;
static const float DISCRETIZATION_TIME = 0.001;

static void CheckCudaErrorAux (const char *, unsigned, const char *, cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)

/**
 * CUDA kernel that computes reciprocal values for a given vector
 */
__global__ void eulerKernel(float *data, float *cache, float *buffer, float *coef, unsigned vectorSize) {
	unsigned idx = blockIdx.x*blockDim.x+threadIdx.x;

	if ( (idx < vectorSize -1) && (idx > 0) ){
		data[idx] = cache[idx] + 1 / coef[idx + idx * vectorSize] *
					( buffer[idx] -
						( cache[idx - 1] * coef[idx - 1 + idx * vectorSize] +
						  cache[idx] 	 * coef[idx 	+ idx * vectorSize] +
						  cache[idx + 1] * coef[idx + 1 + idx * vectorSize] )
					);
	}
	if ( idx == 0 ){
		data[idx] = 0;
	}
	if ( idx == vectorSize ){
		data[idx] = 5;
	}

}

/**
 * Host function that copies the data and launches the work on GPU
 */
float *gpuEuler(float *data, unsigned size, float *coef, unsigned timeSize)
{
	float *rc 		= new float[size];
	float *cpuCache = new float[size];
	float *gpuData, *gpuCache, *gpuBuffer, *gpuCoef;

	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuData, sizeof(float)*size));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuCache, sizeof(float)*size));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuBuffer, sizeof(float)*size));
	CUDA_CHECK_RETURN(cudaMalloc((void **)&gpuCoef, sizeof(float)*size*size));

	CUDA_CHECK_RETURN(cudaMemcpy(gpuCoef, coef, sizeof(float)*size*size, cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(rc, data, sizeof(float)*size, cudaMemcpyHostToHost));
	
	static const int BLOCK_SIZE = 256;
	const int blockCount = (size+BLOCK_SIZE-1)/BLOCK_SIZE;

	int evaluatedEps = 0;

	for (unsigned i = 0; i < timeSize; i++){
		CUDA_CHECK_RETURN(cudaMemcpy(gpuBuffer, rc, sizeof(float)*size,
										cudaMemcpyHostToDevice));			// from Host to Device
																			// from result approved to buffer
		do {
			CUDA_CHECK_RETURN(cudaMemcpy(cpuCache, rc, sizeof(float)*size,
											cudaMemcpyHostToHost));			// from Host to Host
																			// last returned to cache on Host
			CUDA_CHECK_RETURN(cudaMemcpy(gpuCache, cpuCache, sizeof(float)*size,
											cudaMemcpyHostToDevice));		// from Host to GPU
																			// last returned to cache on GPU
			eulerKernel<<<blockCount, BLOCK_SIZE>>> (gpuData, gpuCache, gpuBuffer, gpuCoef, size);
			CUDA_CHECK_RETURN(cudaMemcpy(rc, gpuData, sizeof(float)*size,
											cudaMemcpyDeviceToHost));		// from GPU to Host
			for (unsigned i = 0; i < size; i++) {
				evaluatedEps += abs(cpuCache[i] - rc[i]);
			}
		} while (evaluatedEps > 0.001);
	}

	CUDA_CHECK_RETURN(cudaFree(gpuData));
	CUDA_CHECK_RETURN(cudaFree(gpuCache));
	CUDA_CHECK_RETURN(cudaFree(gpuBuffer));
	CUDA_CHECK_RETURN(cudaFree(gpuCoef));

	return rc;
}

void initialize(float *data, unsigned size)
{
	for (unsigned i = 0; i < size; ++i)
		data[i] = 0;
}

void initializeCoef(float *mtrxPtr, unsigned const size){
	mtrxPtr[0 + size * 0] = 1;
	mtrxPtr[size - 1 + size * (size - 1)] = 1;
	const float A1 = DISCRETIZATION_TIME / ( DISCRETIZATION_LENGTH * DISCRETIZATION_LENGTH );
	const float A2 = 2 * A1 + 1;
	for (unsigned i = 1; i < size - 1; i++){
		mtrxPtr[i + size * (i - 1)] = -1 * A1;
		mtrxPtr[i + size * i] = A2;
		mtrxPtr[i + size * (i + 1)] = -1 * A1;
	}
}

int main(void)
{
	static int WORK_SIZE 	= LENGTH / DISCRETIZATION_LENGTH;
	static int WORK_TIMES 	= TIME / DISCRETIZATION_TIME;
	float *data 		= new float[WORK_SIZE];
	float *matrixCoef 	= new float[WORK_SIZE * WORK_SIZE];

	initializeCoef(matrixCoef, WORK_SIZE);

	float *result = gpuEuler(data, WORK_SIZE, matrixCoef, WORK_TIMES);

	std::cout << WORK_SIZE << "\n";
	initialize (data, WORK_SIZE);

	FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");

	std::cout << result[WORK_SIZE - 2] << "\n";
	fprintf(gnuplotPipe, "plot '-' \n");
	for (int i = 0; i < WORK_SIZE; i++)
	{
		fprintf(gnuplotPipe, "%lf %lf\n", i * DISCRETIZATION_LENGTH, result[i]);
	}

	fprintf(gnuplotPipe, "e");

	delete[] data;
	delete[] matrixCoef;

	return 0;
}

/**
 * Check the return value of the CUDA runtime API call and exit
 * the application if the call has failed.
 */
static void CheckCudaErrorAux (const char *file, unsigned line, const char *statement, cudaError_t err)
{
	if (err == cudaSuccess)
		return;
	std::cerr << statement<<" returned " << cudaGetErrorString(err) << "("<<err<< ") at "<<file<<":"<<line << std::endl;
	exit (1);
}

